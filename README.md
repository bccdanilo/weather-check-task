aaaaa



# Codilime Task
1. Create a Python script called 'HoeWarmIsHetInDelft.py' that retrieves from http://www.weerindelft.nl/ the current temperature in Delft and prints it to standard output, rounded to degrees Celsius. Example expected output: 18 degrees Celsius 
2. Write an appropriate dockerfile to containerize the script developed in point 1 
3. Write a simple pipeline on https://www.gitlab.com that builds the container above and then executes it. We'll review the code based on clarity and correctness. It is important for the code to be robust, run correctly in a pipeline environment and to be easily troubleshootable by other DevOps engineers.


# Explanation why i am using the https://weerindelft.nl/clientraw.txt as link
The https://weerindelft.nl is a little old page (I don't even know if it is working properly) that doesn't have API and uses AJAX. 
Elements are preloaded and then changed dynamically when the page receives a POST from the clientraw
Most of these changes happen inside an iFrame on the main HTML page.
If you update the page a little faster, you will see the value start as 2.4C then it will update the iframe that contains the data.
And, if you check the network tab in the web browser console, you will see a request every ~6s

In the site js methods:

    ---
    **NOTE**
    //  main function.. read clientraw.txt and format <span class="ajax" id="ajax..."></span> areas

    var clientraw = x.responseText.split(' ');
    // now make sure we got the entire clientraw.txt  
    // valid clientraw.txt has '12345' at start and '!!' at end of record
    // If we have a valid clientraw file AND updates is < maxupdates
    if(clientraw[0] == '12345' && wdpattern.test(x.responseText) && 
        ( updates <= maxupdates || maxupdates > 0  ) ) 
        if (maxupdates > 0 ) {updates++; } // increment counter if needed
        //Temperature
        temp = convertTemp(clientraw[4]);
    ---


Example of data received by the clientraw.txt (https://weerindelft.nl/clientraw.txt)

    12345 0.0 0.0 0 12.6 74 1022.9 0.0 0.0 0.0 277.43 0.00 18.3 61 100.0 5 0.0 0 0 0.0 -100.0 255.0 -100.0 -100.0 -100.0 -100.0 -100 -100 -100 10 25 34 weerindelft_-10:25:34 0 0 13 4 0.00 0.00 100 100 100 100 100 12.6 13.0 12.6 12.6 5 Overcast_and_gloomy/Dry -0.6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0 8.1 1860.0 13/4/2024 13.0 10.4 12.6 12.6 0.0 0 0 0 0 0 0 0 0 0 0 12.6 12.6 12.6 12.6 12.6 12.6 12.6 12.6 12.6 12.6 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 12.6 12.6 12.6 0.0 0 --- --- 6 0 0 -100.0 -100.0 -100 -100 -100 -100 -100 0.0 18.7 18.1 12.6 1025.6 1022.9 0 09:00 10:00:49 12.6 11.6 8.1 8.1 0 2024 -11.4 -1 0 0 32407 32407 32407 32407 32407 32407 32407 32407 32407 32407 0.0 255.0 0.0 10.4 51.97944 -4.34611 0.0 74 74 0.0 00:00 0.0 0.0 0.0 0.0 0.0 0.0 0.0 10:03 00:00 7 !!C10.37S143!! 

But why is this important, i can still get the page value using Pyppeteer by starting a web browser and add a delay before get the information
This would be slower and more resource intensive and the result would be the same

I added code for both ways because I don't have a way to confirm which method should I use
But I only sent the HoeWarmIsHetInDelft.py 

You can check my other implementation here -> https://gitlab.com/bccdanilo/weather-check-pyppeteer
Hope that this does not disqualify me :p



# About the DockerFile
It is a very simple container using Python 3.10.12 (the current version of my setup) slim to reduce the memory needed
There is no memory clausula but it is good to reduce it if the slim image contemplates everything that we needed
Since the dockerfile will contain only my script (that is the idea of the container) I chose to not create a virtual environment (VENV).
I also decided to create the requirement files to not add a lot of text into the dockerfile, so it is easier to read


# About the pipeline
The pipeline is also very simple, I didn't specify any stage, since it should run every execution so you can review it
As I am using gitlab.com, I am also using the public Runners available, so I let the docker stable as an image
And add the Docker-in-Docker service (dind) so it does not generate any other problem with the docker commands
The last two lines just built the image and created the container, also passing flags to remove when exited.
