#dockerfile
#i dont like to use latest when specifying docker images because of compatibility issues. Here I wont have any but as good pratice i will let the version 3.10
#i will also use the slim version, it is smaller and have everything that i need for this project

FROM python:3.10.12-slim

LABEL Maintainer="Danilo Rodrigues"

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY HoeWarmIsHetInDelft.py .

CMD [ "python", "./HoeWarmIsHetInDelft.py" ]