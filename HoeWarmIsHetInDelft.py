"""
CodiLime task to get info from site
"""

import requests


def get_site_info():
    """
    Method to get information from the website
    and returning already rounded as requested.
    Please also read the README.mds
    """
    endpoint = "https://weerindelft.nl/clientraw.txt"

    try:
        # timeout to close if the site doesnt respond
        site_output = requests.get(endpoint, timeout=5).text
        # the returned data is a list of values, i took the 5th as temperature
        # please check the readme i will explain it there
        site_data_list = site_output.split()
        temperature = float(site_data_list[4])
        return round(temperature)  # round to print as requested
    except requests.exceptions.RequestException as errors:
        print("Exception with the Request:", errors)
    except Exception as errors:
        print("Other Exception:", errors)
    return None


def main():
    """
    Main method of this code, it will only print the required text
    """
    temperature = get_site_info()
    if temperature:
        print(f"{temperature} degrees Celsius")
    else:
        print("There was an issue on this method")


if __name__ == "__main__":
    main()
